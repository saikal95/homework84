const mongoose = require('mongoose');
const config = require("./config");
const User = require("./models/User");
const Task = require("./models/Task");

const run = async () => {
  await mongoose.connect(config.mongo.db, config.mongo.options);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [John, Paul, Sasha] = await User.create({
    username: 'John',

  }, {
    username: 'Paul',

  }, {
    username: 'Sasha',

  });

  await Task.create(
    {
      user: Paul,
      title: 'Preparation of backend part' ,
      status: 'in_progress',
    },
    {
      user: John,
      title: 'To prepare the HTML/CSS base for website',
      status: 'new' ,
    },
    {
      user: Sasha,
      title: 'To test MVP version',
      status: 'done' ,
    });

  await mongoose.connection.close();
};

run().catch(e => console.error(e));
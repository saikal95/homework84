const express = require('express');
const User = require('../models/User');
const Task = require("../models/Task");
const router = express.Router();

router.get('/', async (req, res, next) => {
  try {
    const query = {};
    const sort = {};

    const tasks = await Task.find(query).sort(sort).populate("user", "username ");

    return res.send(tasks);
  } catch (e) {
    next(e);
  }
});

router.put('/:id', async (req, res, next) => {
  try {
    const taskId = req.params.id;
    const taskSecond  = await Task.findByIdAndUpdate(taskId,  {user: req.body.user, status: req.body.status});

   await taskSecond.save();

   return res.send('Updated task: ' +  taskSecond);
  }catch (e) {
    next(e);
  }

});

router.delete('/:id', async (req, res, next) => {
  try {
    const taskId = req.params.id

    const deletedTask  = await Task.findByIdAndDelete(taskId);


    return res.send('Deleted task: ' +  deletedTask);
  }catch (e) {
    next(e);
  }

});

router.post('/', async (req, res, next) => {
  try {
    console.log(req.body);
    if (!req.body.title ) {
      return res.status(400).send({message: 'Please put the name of the assignment'});
    }

    const taskData = {
      user: req.body.user,
      title: req.body.title,
      status: 'new',
    };


    const task = new Task(taskData);
    await task.save();
    return res.send({message: 'New task created', id: task._id});


  } catch (e) {
    next(e);
  }
});



module.exports = router;



import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {map} from "rxjs";
import {ApiUserData, User} from "../model/user.model";
import {ApiTaskData} from "../model/task.model";


@Injectable({
  providedIn: 'root'
})

export class UserService {
  constructor(private http: HttpClient) {}


  getUsers() {
    return this.http.get<ApiUserData[]>(environment.apiUrl + '/user').pipe(
      map(response => {
        return response.map(userData => {
          return new User(
            userData._id,
            userData.username
          );
        });
      })
    );
  }

}

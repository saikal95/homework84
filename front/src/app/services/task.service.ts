import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {ApiTaskData, Task, TaskData} from "../model/task.model";
import {environment} from "../../environments/environment";
import {map} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class TaskService {
  constructor(private http: HttpClient) {}

  getTasks() {
    return this.http.get<ApiTaskData[]>(environment.apiUrl + '/task').pipe(
      map(response => {
        return response.map(taskData => {
          return new Task(
            taskData._id,
            taskData.user,
            taskData.title,
            taskData.status,
          );
        });
      })
    );
  }

  createTask(taskData: TaskData) {
    return this.http.post(environment.apiUrl + '/task', taskData);
  }


  removeTask(taskId: string) {
    return this.http.delete<{[taskId:string]:ApiTaskData}>(environment.apiUrl + '/task/'+ taskId);
  }



  editTask(taskData: TaskData, id: string) {

    return this.http.put(environment.apiUrl + '/task/'+ id, taskData);
  }


}

import {Component, ViewChild} from '@angular/core';
import {NgForm} from "@angular/forms";
import {Observable} from "rxjs";
import {Store} from "@ngrx/store";
import {AppState} from "../store/types";
import {TaskData} from "../model/task.model";
import {createTaskRequest} from "../store/task.actions";

@Component({
  selector: 'app-new-task',
  templateUrl: './new-task.component.html',
  styleUrls: ['./new-task.component.sass']
})
export class NewTaskComponent  {
  @ViewChild('f') form!: NgForm;
  loading: Observable<boolean>;
  error: Observable<string | null>;


  constructor(
    private store: Store<AppState>
  ) {
    this.loading = store.select(state => state.tasks.createLoading);
    this.error = store.select(state => state.tasks.createError);
  }


  onSubmit() {
    const taskData: TaskData = this.form.value;
    this.store.dispatch(createTaskRequest({taskData}));
  }
}

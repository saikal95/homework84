import {Component, EventEmitter, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {AppState} from "../store/types";
import {Store} from "@ngrx/store";
import {Task, TaskData} from "../model/task.model";
import {createTaskRequest, deleteTasksRequest, editTaskRequest, fetchTasksRequest} from "../store/task.actions";
import {User} from "../model/user.model";
import {fetchUsersRequest} from "../store/user.actions";

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.sass']
})
export class TaskComponent implements OnInit {
  tasks: Observable<Task[]>
  users: Observable<User[]>
  loading: Observable<boolean>
  error: Observable<null | string>

  constructor(private store: Store<AppState>) {
    this.tasks = store.select(state => state.tasks.tasks);
    this.users = store.select(state => state.users.users);
    this.loading = store.select(state => state.tasks.fetchLoading);
    this.error = store.select(state => state.tasks.fetchError);
  }


  ngOnInit(): void {
    this.store.dispatch(fetchTasksRequest());
    this.store.dispatch(fetchUsersRequest());
  }

  onDelete(id: any) {
    this.store.dispatch(deleteTasksRequest({id: id}));
    this.ngOnInit();
  }


  onEdit(event: Event) {
    console.log(event.target);
   const finalValue =  <HTMLInputElement>event.target
    const anotherReq = finalValue.value;
    console.log(finalValue.value);
    // this.store.dispatch(editTaskRequest)


  }};


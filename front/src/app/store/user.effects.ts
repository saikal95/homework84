import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {catchError, map, mergeMap, of} from "rxjs";
import {fetchUsersFailure, fetchUsersRequest, fetchUsersSuccess} from "./user.actions";
import {UserService} from "../services/user.service";


@Injectable()
export class UserEffects {
  constructor(
    private actions: Actions,
    private userService: UserService,
   ) {}

  fetchUsers = createEffect(() => this.actions.pipe(
    ofType(fetchUsersRequest),
    mergeMap(()=> this.userService.getUsers().pipe(
      map(users => fetchUsersSuccess({users})),
      catchError(()=> of(fetchUsersFailure({
        error: 'Something went wrong',
      })))
    ))

  ));



}

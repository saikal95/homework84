import { createAction, props } from '@ngrx/store';
import {Task, TaskData} from "../model/task.model";



export const fetchTasksRequest = createAction('[Task] Fetch Request',
);


export const fetchTasksSuccess = createAction(
  '[Task] Fetch Success',
  props<{tasks: Task[]}>()
);
export const fetchTasksFailure = createAction(
  '[Task] Fetch Failure',
  props<{error: string}>()
);

export const createTaskRequest = createAction(
  '[Task] Create Request',
  props<{taskData: TaskData}>()
);
export const createTaskSuccess = createAction(
  '[Task] Create Success'
);
export const createTaskFailure = createAction(
  '[Task] Create Failure',
  props<{error: string}>()
);

export const deleteTasksRequest = createAction('[Task] Delete Request',
  props<{id: string}>()
);

export const deleteTasksSuccess = createAction(
  '[Task] Delete Success',
);
export const deleteTasksFailure = createAction(
  '[Task] Delete Failure',
  props<{error: string}>()
);

export const editTaskRequest = createAction('[User] Edit Request',
  props<{task: Task}>()
);

export const editTaskSuccess = createAction(
  '[User] Edit Success',
);
export const editTaskFailure = createAction(
  '[User] Edit Failure',
  props<{error: string}>()
);



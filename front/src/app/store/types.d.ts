import {Task} from "../model/task.model";
import {User } from "../model/user.model";


export type TaskState = {
  tasks: Task[],
  fetchLoading: boolean,
  fetchError: null | string,
  createLoading: boolean,
  createError: null | string,
  deleteLoading: boolean,
  deleteError: null | string,
};

export type AppState = {
  tasks: TaskState,
  users: UserState
}

export type UserState = {
  users: User[],
  fetchLoading: boolean,
  fetchError: null | string,
};


import {createReducer, on} from "@ngrx/store";
import {TaskState} from "./types";
import {
  createTaskFailure,
  createTaskRequest,
  createTaskSuccess,
  deleteTasksFailure,
  deleteTasksRequest,
  deleteTasksSuccess,
  editTaskFailure,
  editTaskRequest,
  editTaskSuccess,
  fetchTasksFailure,
  fetchTasksRequest,
  fetchTasksSuccess
} from "./task.actions";


const  initialState: TaskState = {
  tasks: [],
  fetchLoading: false,
  fetchError: <null | string>null,
  createLoading: false,
  createError: null,
  deleteLoading: false,
  deleteError: null,
}
export const taskReducer = createReducer(
  initialState,

  on(fetchTasksRequest, state => ({...state, fetchLoading: true})),
  on(fetchTasksSuccess, (state, {tasks}) => ({
    ...state,
    fetchLoading: false,
    tasks
  })),
  on(fetchTasksFailure, (state, {error}) => ({
    ...state,
    fetchLoading: false,
    fetchError: error,
  })),
  on(createTaskRequest, state => ({...state, createLoading: true})),
  on(createTaskSuccess, state => ({...state, createLoading: false})),
  on(createTaskFailure, (state, {error}) => ({
    ...state,
    createLoading: false,
    createError: error,
  })),
  on(deleteTasksRequest, state => ({...state, deleteLoading: true})),
  on(deleteTasksSuccess, state => ({...state, deleteLoading: false})),
  on(deleteTasksFailure, (state, {error}) => ({
    ...state,
    deleteLoading: false,
    deleteError: error,
  })),
  on(editTaskRequest, state => ({...state, deleteLoading: true})),
  on(editTaskSuccess, state => ({...state, deleteLoading: false})),
  on(editTaskFailure, (state, {error}) => ({
    ...state,
    deleteLoading: false,
    deleteError: error,
  })),



);

import {createAction, props} from "@ngrx/store";
import {User} from "../model/user.model";

export const fetchUsersRequest = createAction('[User] Fetch Request',
);
export const fetchUsersSuccess = createAction(
  '[User] Fetch Success',
  props<{users: User[]}>()
);
export const fetchUsersFailure = createAction(
  '[User] Fetch Failure',
  props<{error: string}>()
);


import {Actions, createEffect, ofType} from "@ngrx/effects";
import {Injectable} from "@angular/core";
import {TaskService} from "../services/task.service";
import {catchError, map, mergeMap, of, tap} from "rxjs";
import {
  createTaskFailure,
  createTaskRequest, createTaskSuccess,
  deleteTasksFailure,
  deleteTasksRequest,
  deleteTasksSuccess, editTaskRequest, editTaskSuccess,
  fetchTasksFailure,
  fetchTasksRequest,
  fetchTasksSuccess
} from "./task.actions";
import {Router} from "@angular/router";


@Injectable()
export class TaskEffects {
  constructor(
    private actions: Actions,
    private taskService: TaskService,
    private router: Router) {}

  fetchTasks = createEffect(() => this.actions.pipe(
    ofType(fetchTasksRequest),
    mergeMap(()=> this.taskService.getTasks().pipe(
      map(tasks => fetchTasksSuccess({tasks})),
      catchError(()=> of(fetchTasksFailure({
        error: 'Something went wrong',
      })))
    ))

  ));

  deleteTask = createEffect(() => this.actions.pipe(
    ofType(deleteTasksRequest),
    mergeMap(({id}) => this.taskService.removeTask(id).pipe(
      map(() => deleteTasksSuccess()),
      catchError(() => of(deleteTasksFailure({error: 'Wrong data'}))))
    ))
  );

  createTask = createEffect(() => this.actions.pipe(
    ofType(createTaskRequest),
    mergeMap(({taskData}) => this.taskService.createTask(taskData).pipe(
      map(() => createTaskSuccess()),
      tap(() => this.router.navigate(['/'])),
      catchError(() => of(createTaskFailure({error: 'Wrong data'})))
    ))
  ));

  editTask = createEffect(() => this.actions.pipe(
    ofType(editTaskRequest),
    mergeMap(({task}) => this.taskService.editTask(task, task.id).pipe(
      map(() => editTaskSuccess()),
      catchError(() => of(deleteTasksFailure({error: 'Wrong data'}))))
    ))
  );

}
